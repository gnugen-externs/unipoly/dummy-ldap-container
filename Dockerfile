# Dockerfile for the openldap container used in our CI process.
# Based on https://github.com/osixia/docker-openldap

FROM osixia/openldap:1.3.0
MAINTAINER Timothée Floure <timothee.floure@epfl.ch>

# Applied when the container start:
# see https://github.com/osixia/docker-openldap#extend-osixiaopenldap121-image
ADD ldif /container/service/slapd/assets/config/bootstrap/ldif/custom
