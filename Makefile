IMAGE="gitlab.gnugen.ch:5005/gnugen-externs/unipoly/dummy-ldap-container/unipoly-dummy-ldap:latest"
LDAP_ORGANISATION="Unipoly"
LDAP_DOMAIN="unipoly.epfl.ch"
LDAP_ADMIN_PASSWORD="admin"
CONTAINER_RUNTIME="/usr/bin/podman"

all:
	@echo "make build: build with docker build"
	@echo "make run: run the configured image with docker"

build:
	$(CONTAINER_RUNTIME) build -t $(IMAGE) .

run:
	$(CONTAINER_RUNTIME) run \
		--publish=3089:389 \
		--env LDAP_ORGANISATION=$(LDAP_ORGANISATION) \
		--env LDAP_DOMAIN=$(LDAP_DOMAIN) \
		--env LDAP_ADMIN_PASSWORD=$(LDAP_ADMIN_PASSWORD) $(IMAGE) --loglevel debug
