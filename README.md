# Dummy Unipoly LDAP

This container is used for development and testing of unipoly's utilities,
based on [osixia's docker-openldap](https://github.com/osixia/docker-openldap)
image. The default password is `admin` for `cn=admin,dc=unipoly,dc=epfl,dc=ch`
and `password` for any other user.
